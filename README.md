# GO cli program for playing media

## REQUIREMENTS
- [mpv](https://mpv.io/) to play files
- valid config file (see [INSTALL](#INSTALL))

## INSTALL
- `sudo make install`
    - install gotv in /usr/local/bin/
    - copy gotv.1 to /usr/local/share/man/man1
- `make config`
    - copy example gotv.conf to ~/.config/gotv/
    - for more info about config see *man(1) gotv*

## FOLDER STRUCTURE

### program expects this folder structure:

- root/
    - category1/
        - item1/
            - file1
            - file2
        - file1
    - category2/
        - item1/
            - subfolder1/
                - file1
        - item2
            - file1
            - file2

### example
- tv/
    - shows/
        - himym/
            - eps01.01.mp4
            - eps01.02.mp4
        - tbbt/
            - s03/
                - eps3.mkv
                - eps3.srt
                - eps4.mkv
                - eps4.srt
        - arcane_s01_e01.mkv
    - movies/
        - cars/
            - cars.avi
            - cars.srt
        - 12_angry_men.mkv
        - 12_angry_men.srt
