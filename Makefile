DESTDIR=/usr/local/bin
MANDIR=/usr/local/share/man

gotv:
	go build

config:
	mkdir -p ${HOME}/.config/gotv
	cp -i gotv.conf ${HOME}/.config/gotv/gotv.conf

install: 
	mkdir -p $(DESTDIR)
	go build -o $(DESTDIR)
	chmod 775 $(DESTDIR)/gotv
	mkdir -p $(MANDIR)/man1
	cp gotv.1 $(MANDIR)/man1
	chmod 644 $(MANDIR)/man1/gotv.1
	go build -o $(DESTDIR) ./gotv-rename 
	chmod 775 $(DESTDIR)/gotv-rename

uninstall:
	rm -f $(DESTDIR)/gotv
	rm -f $(DESTDIR)/man1/gotv.1
	rm -f $(DESTDIR)/gotv-rename

clean:
	go clean
	go clean -modcache
