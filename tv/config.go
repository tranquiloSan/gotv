package tv

// parsing config file (~/.config/gotv/gotv.conf)

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"

	"github.com/BurntSushi/toml"
)

type config struct {
	TVDir string
	MPV   mpv   `toml:"mpv"`
	Mount mount `toml:"mount"`
}

type mpv struct {
	Exec string
	Args []string
}

type mount struct {
	Enabled    bool
	Type       string
	Share      string
	Mountpoint string
	UID        int
	GID        int
	User       string
	Pass       string
	CMD        string
}

// Config of gotv
var Config config

func init() {
	// find user config folder
	userConfigDir, err := os.UserConfigDir()
	if err != nil {
		logger.Fatalf("Could not find user config directory!\n")
	}

	// get config file
	var configFile = filepath.Join(userConfigDir, "gotv/", "gotv.conf")

	// parse config as TOML file
	_, err = toml.Decode(fileToString(configFile), &Config)
	if err != nil {
		logger.Fatalf("Error while loading config %q (%v)", configFile, err)
	}

	// Add mount command with included config
	Config.Mount.CMD = fmt.Sprintf(
		"mount -t %s %s %s -o uid=%d,gid=%d,username=%s,password=%s",
		Config.Mount.Type, Config.Mount.Share, Config.Mount.Mountpoint,
		Config.Mount.UID, Config.Mount.GID, Config.Mount.User, Config.Mount.Pass,
	)
}

// returns file as string
func fileToString(path string) string {
	bytes, err := ioutil.ReadFile(path)
	if err != nil {
		logger.Fatalf("Could not read config %q (%v)", path, err)
	}

	return string(bytes)
}
