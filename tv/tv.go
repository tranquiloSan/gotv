package tv

// actual gotv functionality
// (choosing, playing and deleting media)

import (
	"fmt"
	"io/fs"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"syscall"

	fuzzyfinder "github.com/ktr0731/go-fuzzyfinder"
)

var logger = log.New(os.Stderr, "", 0)

// Struct that holds information about disk space
type DiskStatus struct {
	Size  float64
	Used  float64
	Avail float64
}

// Calculate disk space (Size, Used, Avail)
func DiskSpace() (disk DiskStatus) {
	toGB := func(bytes uint64) float64 {
		return float64(bytes) / float64(1024*1024*1024)
	}

	fs := syscall.Statfs_t{}
	err := syscall.Statfs(Config.TVDir, &fs)
	if err != nil {
		return
	}

	disk.Size = toGB(fs.Blocks * uint64(fs.Bsize))
	disk.Avail = toGB(fs.Bfree * uint64(fs.Bsize))
	disk.Used = disk.Size - disk.Avail
	return
}

// formated print of files (paths are relative to `root`):
// LIST (`root`)
// - fileX
// - fileY
// Total of [`total`] file(s)
func List(files []string, root string) {
	// print `root` to list relative to TVDIR (or absolute path if `root` == TVDIR)
	rel, err := filepath.Rel(Config.TVDir, root)
	if err != nil || rel == "." {
		fmt.Printf("List %s\n", root)
	} else {
		fmt.Printf("List %s\n", rel)
	}

	// print `files` relative to `root` dir
	for _, file := range files {
		rel, err := filepath.Rel(root, file)
		if err != nil {
			fmt.Println("-", file)
		} else {
			fmt.Println("-", rel)
		}
	}

	// print number of files listed
	total := len(files)
	f := "files"
	if total == 1 {
		f = "file"
	}
	fmt.Printf("Total of %v %v\n", total, f)
}

// Mount with shell
func Mount() (out []byte, err error) {
	mount := exec.Command("/bin/sh", "-c", "sudo "+Config.Mount.CMD)
	out, err = mount.Output()
	return
}

// Play files in mpv, returns slice of played files
// save is true if mpv save on exit
func Play(item string) (played []string, save bool) {
	// Play in mpv
	/* the `append`
	   creates slice with `item`
	   then appends all items from MPV_ARGS
	   then uses these items as args to `exec.Command()`
	*/
	cmd := exec.Command(Config.MPV.Exec, append([]string{item}, Config.MPV.Args...)...)

	// Read stdout from mpv and get only filenames
	out, err := cmd.Output()

	if err != nil {
		logger.Fatalf("Error while playing %q (%v)\n", item, err)
	}

	// When exiting mpv with saving state, end program
	save_rgx := regexp.MustCompile("Saving state.")
	if len(save_rgx.FindStringSubmatch(string(out))) != 0 {
		return nil, true
	}

	// if file is not directory return only the file
	if stat, _ := os.Stat(item); !stat.IsDir() {
		return []string{item}, false
	}

	// get all files that were playing
	rgx := regexp.MustCompile("Playing: (.+)")
	matches := rgx.FindAllStringSubmatch(string(out), -1)

	for _, match := range matches {
		played = append(played, match[1])
	}

	// fix played to be only media
	KeepMedia(&played)

	return played, false
}

// Choose media files to delete from `files`
func Delete(files []string) (deleted []string, err error) {
	// append extra options for deletion

	var deletePrompt string
	var choices []string

	if len(files) == 1 {
		deletePrompt = fmt.Sprintf("Delete %s? ", filepath.Base(files[0]))
		choices = []string{"[NO]", "[YES]"}
	} else if len(files) > 1 {
		deletePrompt = "To delete: "
		choices = append([]string{"[NONE]", "[ALL_BUT_LAST]", "[ALL]"}, files...)
	}

	// choose whats to be deleted
	idxs, err := fuzzyfinder.FindMulti(
		choices,
		func(i int) string {
			return fmt.Sprint(filepath.Base(choices[i]))
		},
		fuzzyfinder.WithPromptString(deletePrompt),
	)
	if err != nil {
		return nil, nil
	}

	// choose files to delete
	var toDelete []string
	for _, i := range idxs {
		toDelete = append(toDelete, choices[i])
	}

	// edit toDelete according to choice
	if len(files) > 0 {
		if contains(toDelete, "[NONE]", "[NO]") {
			toDelete = nil
		} else if contains(toDelete, "[ALL_BUT_LAST]") {
			toDelete = files[:len(files)-1]
		} else if contains(toDelete, "[ALL]", "[YES]") {
			toDelete = files
		}
	}

	if len(toDelete) > 0 {
		// fmt.Println("Deleting:")
		for _, file := range toDelete {
			// remove extension from file
			fileNoExt := strings.TrimSuffix(file, filepath.Ext(file))

			// get all files with name `fileNoExt`
			// without caring about extension
			fileNoExt = strings.Replace(fileNoExt, "[", "\\[", -1)
			fileNoExt = strings.Replace(fileNoExt, "]", "\\]", -1)
			allFiles, err := filepath.Glob(fileNoExt + "*")
			if err != nil {
				return nil, err
			}

			// go through all files and delete them
			for _, f := range allFiles {
				err = os.Remove(f)
				if err == nil {
					deleted = append(deleted, f)
					// fmt.Println("-", filepath.Base(f))
				} else {
					logger.Printf("err: %q\n", err)
				}
			}
		}
	}

	return deleted, nil
}

// Remove empty directories in path (root included)
func ClearDirs(path string) (deleted []string) {
	// if path is file or path already deleted do nothing
	if stat, err := os.Stat(path); err != nil || !stat.IsDir() {
		return
	}

	// remove empty directories
	var dirs []string
	filepath.WalkDir(path, func(path string, d fs.DirEntry, err error) error {
		if d.IsDir() {
			dirs = append(dirs, path)
		}
		return nil
	})
	dirs = append(dirs, path)

	// going backwards because you need to remove nested dirs first
	for i := len(dirs) - 1; i > 0; i-- {
		err := os.Remove(dirs[i])
		if err == nil {
			deleted = append(deleted, dirs[i])
			// fmt.Printf("Deleting empty directory %s", filepath.Base(dirs[i]))
		}
	}

	return
}
