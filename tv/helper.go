package tv

// helper functions

import (
	"fmt"
	"mime"
	"os"
	"path/filepath"
	"strings"

	fuzzyfinder "github.com/ktr0731/go-fuzzyfinder"
	fuzzy "github.com/lithammer/fuzzysearch/fuzzy"
)

// fuzzy search, returns matched string and error
func Fuzzy(files []string, prompt string, searchString string) (string, error) {
	// nothing to search
	if len(files) == 0 {
		return "", fuzzyfinder.ErrAbort
	}

	// search by string and not user input
	if searchString != "" {
		dir := filepath.Dir(files[0])
		ConvertToBaseNames(&files)

		found := fuzzy.Find(searchString, files)

		if len(found) == 0 {
			return "", fuzzyfinder.ErrAbort
		}

		return filepath.Join(dir, found[0]), nil
	}

	// normal fzf search
	idx, err := fuzzyfinder.Find(
		files,
		func(i int) string {
			return fmt.Sprint(filepath.Base(files[i]))
		},
		fuzzyfinder.WithPromptString(prompt),
		fuzzyfinder.WithHeader(os.Args[0]),
	)
	return files[idx], err
}

// Return all (not hidden) files in a folder `path`
func GetFiles(path string) (files []string, err error) {
	files, err = filepath.Glob(path + "/[^.]*")
	return
}

// takes slice of files and changes them to basenames only
func ConvertToBaseNames(files *[]string) {
	for i, file := range *files {
		(*files)[i] = filepath.Base(file)
	}
}

// takes slice of files and removes files that are not audio/video
func KeepMedia(files *[]string) {
	var keep_files []string
	for _, file := range *files {
		mimetype := mime.TypeByExtension(filepath.Ext(file))
		// check if files is not video and or audio
		if strings.HasPrefix(mimetype, "video") ||
			strings.HasPrefix(mimetype, "audio") {
			// remove item on index `i` from `files`
			keep_files = append(keep_files, file)
		}
	}
	*files = keep_files
}

// checks if slice contanis any match
func contains(slice []string, matches ...string) bool {
	for _, value := range slice {
		for _, match := range matches {
			if value == match {
				return true
			}
		}
	}
	return false
}
