package main

// main app

import (
	"fmt"
	"io/fs"
	"log"
	"os"

	"path/filepath"

	"gitlab.com/tranquiloSan/gotv/tv"
)

var logger = log.New(os.Stderr, "", 0)

// print files in formated fasion
func printFiles(files []string, prompt string, baseName bool) {
	if len(files) == 1 {
		file := files[0]
		if baseName {
			file = filepath.Base(file)
		}
		fmt.Printf("%s %s\n", prompt, file)
	} else if len(files) > 1 {
		fmt.Printf("%s:\n", prompt)
		for _, file := range files {
			if baseName {
				file = filepath.Base(file)
			}
			fmt.Println("-", file)
		}
	}
}

func main() {
	// if --ask-on-exit ask for return key at the end of main
	defer func() {
		if opt.AskOnExit {
			fmt.Printf("Press return to exit...")
			fmt.Scanln()
		}
	}()

	var files []string
	var err error

	// folder does not exist
	if _, err := os.Stat(tv.Config.TVDir); os.IsNotExist(err) {
		// if mount enabled, try to mount it
		if tv.Config.Mount.Enabled {
			fmt.Printf("Mounting %s on %s\n", tv.Config.Mount.Share, tv.Config.Mount.Mountpoint)
			_, err = tv.Mount()
			if err != nil {
				logger.Fatalf("Error while mounting (%v)\n", err)
			}
		} else {
			logger.Fatalf("File %q does not exist (%v)\n", tv.Config.TVDir, err)
		}
	}

	if opt.DiskStatus {
		disk := tv.DiskSpace()
		fmt.Printf("Avail: %.1fG, Use%%: %d%% [%.1fG/%.1fG]\n",
			disk.Avail, uint64(disk.Used/disk.Size*100), disk.Used, disk.Size)
		return
	}

	// get files in root dir
	files, err = tv.GetFiles(tv.Config.TVDir)
	if err != nil {
		logger.Fatalln(err)
	}

	// list tv (no category chosen)
	if opt.List && opt.Category == "" {
		tv.List(files, tv.Config.TVDir)
		return
	}

	// select category
	var category string
	category, err = tv.Fuzzy(files, "Choose category: ", opt.Category)
	if err != nil {
		logger.Println("No category selected!")
		return
	}

	// get files in category
	files, err = tv.GetFiles(category)
	if err != nil {
		logger.Fatalln(err)
	}

	// list categories (no item chosen)
	if opt.List && opt.Item == "" {
		tv.List(files, category)
		return
	}

	// select item
	var item string
	item, err = tv.Fuzzy(files, "Choose item: ", opt.Item)
	if err != nil {
		logger.Println("No item selected!")
		return
	}

	// list item (shows media files)
	if opt.List {
		var toList []string
		filepath.WalkDir(item, func(path string, d fs.DirEntry, err error) error {
			toList = append(toList, filepath.Base(path))
			return nil
		})
		tv.KeepMedia(&toList)

		tv.List(toList, item)
		return
	}

	// playing
	rel, err := filepath.Rel(tv.Config.TVDir, item)
	if err != nil {
		fmt.Printf("Playing %s\n", item)
	} else {
		fmt.Printf("Playing %s\n", rel)
	}

	played, save := tv.Play(item)
	if save {
		fmt.Println("Saving state.")
		return
	}
	printFiles(played, "Played", true)

	// deleting
	deleted, err := tv.Delete(played)
	if err != nil {
		logger.Fatalln(err)
	}
	printFiles(deleted, "Deleted", true)

	// cleaning
	deleted = tv.ClearDirs(item)
	printFiles(deleted, "Deleted empty", false)
}
