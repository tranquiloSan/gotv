package main

// dealing with arguments from command line

import (
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/tranquiloSan/gotv/tv"
)

// commandline arguments
var opt struct {
	AskOnExit  bool
	Autoplay   bool
	Category   string
	ConfigFile string
	DiskStatus bool
	Item       string
	List       bool
}

// customize flag.Usage
func editUsage() {
	flag.Usage = func() {
		w := flag.CommandLine.Output()

		fmt.Fprintf(w, "Usage of %s:\n  (all flags are optional)\n", os.Args[0])

		flag.PrintDefaults()

		fmt.Fprintln(w, "  (-c and -i can be positional args)")
		fmt.Fprintln(w, "Config file location: ~/.config/gotv/gotv.conf")

	}
}

// parse arguments from user
func parseArgs() {
	flag.BoolVar(&opt.AskOnExit, "ask-on-exit", false, "ask for return on exit")
	flag.BoolVar(&opt.Autoplay, "a", false, "automatically play next file in playlist (experimental, use with caution)")
	flag.StringVar(&opt.Category, "c", "", "choose category")
	flag.StringVar(&opt.Item, "i", "", "choose item")
	flag.BoolVar(&opt.List, "l", false, "only list")
	flag.BoolVar(&opt.DiskStatus, "df", false, "show disk status")
	//TODO: add possibility to change config file path
	//flag.StringVar(&opt.ConfigFile, "config", "", "custom path to config file")
	flag.Parse()

	// FIXME: dependant on mpv args
	if opt.Autoplay {
		tv.Config.MPV.Args = tv.Config.MPV.Args[3:]
	}

	// named arguments (either -c or -i)
	if len(os.Args) > 1 {
		for _, arg := range os.Args[1:] {
			if !strings.HasPrefix(arg, "-") {
				if opt.Category == "" {
					opt.Category = arg
				} else if opt.Item == "" {
					opt.Item = arg
				} else {
					logger.Fatalln("unknown argument:", arg)
				}
			}
		}
	}
}

func init() {
	editUsage()
	parseArgs()
}
