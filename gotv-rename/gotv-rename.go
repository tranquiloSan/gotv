package main

import (
	"flag"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

const (
	SEASON  = "%S"
	EPISODE = "%E"
)

// replace string case insensitive
func replaceAll(s string, old string, new string) string {
	s = strings.ReplaceAll(s, old, new)
	s = strings.ReplaceAll(s, strings.ToLower(old), new)
	return s
}

func Rename(path string, format_in string, format_out string) {
	// get all files recursively in path
	var files []string
	filepath.WalkDir(path, func(path string, d fs.DirEntry, err error) error {
		if fileInfo, _ := os.Stat(path); fileInfo.IsDir() {
			return nil
		}
		files = append(files, path)
		return nil
	})

	// replace format_in to be regex
	format_in = replaceAll(format_in, SEASON, "(?P<Season>[0-9]+)")
	format_in = replaceAll(format_in, EPISODE, "(?P<Episode>[0-9]+)")
	format_in = ".*" + format_in + ".*"
	rgx := regexp.MustCompile(format_in)

	// rename files
	for _, file := range files {
		// get name of file
		fmt.Println(file)
		name := filepath.Base(file)
		ext := filepath.Ext(file)

		// find season and episode
		matches := rgx.FindStringSubmatch(name)
		if matches == nil {
			fmt.Println("Could not find input format in filename.")
			continue
		}

		season := matches[rgx.SubexpIndex("Season")]
		if len(season) == 1 {
			season = "0" + season
		}

		episode := matches[rgx.SubexpIndex("Episode")]
		if len(episode) == 1 {
			episode = "0" + episode
		}

		// create new file name
		new_name := replaceAll(format_out, SEASON, season)
		new_name = replaceAll(new_name, EPISODE, episode)
		new_name += ext
		new_file := strings.Replace(file, name, new_name, 1)

		// rename file
		err := os.Rename(file, new_file)
		if err != nil {
			fmt.Println("Could not rename file.")
			continue
		}
		fmt.Println("->", new_file)
	}
}

func init() {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage of %s:\n", os.Args[0])
		fmt.Fprintf(os.Stderr, "Recursively renames files.\n\n")

		flag.PrintDefaults()

		fmt.Fprintf(os.Stderr, "\nUse %%S to replace season number\n")
		fmt.Fprintf(os.Stderr, "Use %%E to replace episode number\n\n")

        fmt.Fprintf(os.Stderr, "Example:\n")
        //fmt.Fprintf(os.Stderr, "%s /path/to/folder\n", os.Args[0])
        fmt.Fprintf(os.Stderr, "%s -p /path/to/folder\n", os.Args[0])
        fmt.Fprintf(os.Stderr, "%s -p /path/to/folder -i S%%sE%%e -o eps%%S.%%E\n", os.Args[0])
	}
}

func main() {
	var (
		path       = flag.String("p", "", "Path to folder")
		format_in  = flag.String("i", "S%SE%E", "Input format")
		format_out = flag.String("o", "eps%S.%E", "Output format")
	)
	flag.Parse()

	if *path == "" {
		fmt.Println("Path argument (-p string) is required!")
		return
	}

	Rename(*path, *format_in, *format_out)
}
